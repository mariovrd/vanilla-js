

document.getElementById("add_image__input").addEventListener("click", () => addImage());

function isImageUrlValid(imageUrl) {
    let linkRegex = /((\bhttp?:\/\/)|(\bwww\.))\S+/;
    if (linkRegex.test(imageUrl)) {
        return true;      
    } 
    return false;
}

function addImage() {
    let imageUrl = document.getElementById("image_url__input").value;
    if (isImageUrlValid(imageUrl)) {
        document.getElementById("settings_error").innerHTML = "";        
        let imageCaption = document.getElementById("image_caption__input").value;
        document.getElementById("gallery").innerHTML += 
            "<div class=\"col-3 gallery-item\">" + 
                "<img src=\"" + imageUrl + "\" class=\"gallery-img\">" +
                "<div class=\"caption\">" + imageCaption + "</div>" +
            "</div>";
    } else {
        document.getElementById("settings_error").innerHTML = "Link nije valjan";     
    }
}