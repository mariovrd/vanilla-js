let documentTitle = "Create Your Own Website";
let logoText = "lG";
let logoImage = "";
let heading = document.getElementById("heading").innerText;
let slogan = "Slogan";
let textColor = "#333";
let linkColor = "#f35";

let settingsForm = document.settings;

document.getElementById("document_title__input").addEventListener("input", () => setDocumentTitle());

// decide which input to show - logo_text or logo_image => based on radio input
let logo = settingsForm.logo;
for (let i = 0; i < logo.length; ++i) {
    logo[i].addEventListener("change", () => showLogoInput(settingsForm.logo[i].value));
}

document.getElementById("heading__input").addEventListener("input", () => setHeading());
document.getElementById("slogan__input").addEventListener("input", () => setSlogan());

let aboutTitle = document.getElementById("about_title");
let aboutTitleInput = document.getElementById("about_title__input");
aboutTitleInput.addEventListener("input", () => setElementText(aboutTitle, aboutTitleInput.value));

let aboutText = document.getElementById("about_text");
let aboutTextInput = document.getElementById("about_text__input");
aboutTextInput.addEventListener("input", () => setElementText(aboutText, aboutTextInput.value));

document.getElementById("text_color__input").addEventListener("change", () => setTextColor());
document.getElementById("link_color__input").addEventListener("change", () => setLinkColor());

let facebook = document.getElementById("facebook");
let facebookInput = document.getElementById("facebook__input");
facebookInput.addEventListener("input", () => setLink(facebook, facebookInput.value));

let twitter = document.getElementById("twitter");
let twitterInput = document.getElementById("twitter__input");
twitterInput.addEventListener("input", () => setLink(twitter, twitterInput.value));

let linkedin = document.getElementById("linkedin");
let linkedinInput = document.getElementById("linkedin__input");
linkedinInput.addEventListener("input", () => setLink(linkedin, linkedinInput.value));

function setDocumentTitle() {
    document.title = settingsForm.document_title.value;
}

function showLogoInput(logoId) {
    if (logoId == "logo_text") {
        document.getElementById("logo_text__input").classList.remove("display-none");
        document.getElementById("logo_image__input").classList.add("display-none");
        document.getElementById("logo_text__input").addEventListener("input", () => setLogoText());
        document.getElementById("logo_image__input").removeEventListener("input", () => setLogoImage());
        document.getElementById("logo").innerHTML = settingsForm.logo_text__input.value;
    }
    else {
        document.getElementById("logo_text__input").classList.add("display-none");
        document.getElementById("logo_image__input").classList.remove("display-none");
        document.getElementById("logo_text__input").removeEventListener("input", () => setLogoText());
        document.getElementById("logo_image__input").addEventListener("input", () => setLogoImage());
        document.getElementById("logo").innerHTML = "<img src=\"" + settingsForm.logo_image__input.value + "\" alt=\"logo\">";
    }
}

function setElementText(element, text) {
    element.innerHTML = text;
}

function setLogoText() {
    document.getElementById("logo").innerHTML = settingsForm.logo_text.value;
}

function setLogoImage() {
    let linkRegex = /((\bhttp?:\/\/)|(\bwww\.))\S+/;
    let link = settingsForm.logo_image.value;
    if (linkRegex.test(link)) {
        document.getElementById("logo").innerHTML = "<img src=\"" + link + "\" alt=\"logo\">";
        document.getElementById("settings_error").innerHTML = "";        
    } else {
        document.getElementById("settings_error").innerHTML = "Link za logo nije valjan";        
    }
}

function setHeading() {
    document.getElementById("heading").innerHTML = settingsForm.heading.value;
}

function setSlogan() {
    document.getElementById("slogan").innerHTML = settingsForm.slogan.value;
}

function setTextColor() {
    textColor = settingsForm.text_color.value;
    document.body.style.color = textColor;
}

function setLinkColor() {
    linkColor = settingsForm.link_color.value;
    let links = document.getElementsByTagName("a");
    for (let i = 0; i < links.length; ++i) {
        links[i].style.color = linkColor;
    }
}

function setLink(element, link) {
    let linkRegex = /((\bhttp?:\/\/)|(\bwww\.))\S+/;
    if (linkRegex.test(link)) {
        element.href = link;
        document.getElementById("settings_error").innerHTML = "";        
    } else {
        document.getElementById("settings_error").innerHTML = "Link nije valjan";        
    }
}


